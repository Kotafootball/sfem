program main
use module
implicit none
integer c1, c2, pn, e, ep, i, j,hghl
integer,allocatable::globalnum(:,:)
real(8),allocatable::x(:,:),y(:,:)
real(8) a,b,y1,y2,p1,p2,hg,hl

hghl = 2  !比
c2 = 6  !!separate number of local,error
hl = 2.0d0 / c2  !1 elem length
hg = hghl * hl
c1 = anint(6.0d0 / hg)  !!separate number of global

a = 6.00d0  !!yoko
b = 6.00d0  !!tate
y1 = 18.100d0
y2 = 2.670d0
p1 = 0.26d0
p2 = 0.41d0

pn = (c1+1) * (c1+1)
allocate(x(c1+1,c1+1), y(c1+1,c1+1))
do i = 1, c1 + 1
    do j = 1, c1 + 1
        x(i,j) = (a / c1) * (i - 1)
        y(i,j) = (b / c1) * (j - 1)
    end do
end do

e = c1 * c1
ep = 4
allocate(globalnum(e,ep))
do i = 1, c1
    do j = 1, c1
        globalnum(j + c1 * (i - 1), 1) = j + (c1+1) * (i - 1)
        globalnum(j + c1 * (i - 1), 2) = j + (c1+1) * (i - 1) + 1
        globalnum(j + c1 * (i - 1), 3) = j + (c1+1) * i + 1
        globalnum(j + c1 * (i - 1), 4) = j + (c1+1) * i
    end do
end do

open(30, file = "nodeglobal.dat", status = "replace")
    write(30,*) pn
    do j = 1, c1 + 1
        do i = 1, c1 + 1
            write(30,*)x(i,j),y(i,j)
        end do
    end do
close (30)

open(31, file = "elemglobal.dat", status = "replace")
    write(31,*) e,ep
    do i = 1, e
        write(31,*)globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
    end do
close (31)

open(32, file = "input.dat", status = "replace")
    write(32,*) y1,p1
    write(32,*) y2,p2
    write(32,*) a,b
    write(32,*)c1,c2
close (32)

call localmesh(c2)
call errormesh2(c2)


end program