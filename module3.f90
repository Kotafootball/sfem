module module3
    use module
    use module2
    implicit none

contains

subroutine index(a,b,c1,e1,e,pn2,n,globalnum,indexmat)
    implicit none
    integer,allocatable::globalnum(:,:),mat(:)
    integer,pointer::indexmat(:)
    integer e,pn,ep,i,j,c1,k,l,m,e1,e2,pn2,add,add1,enn1,en,enmax(4),check,check2
    real(8) p,y,a,b,cordx,cordy,xmax,xmin,ymax,ymin,quad(4,2)
    real(8), allocatable ::n(:,:)
    allocate(indexmat(e+1),source = 0)
    allocate(mat(pn2))
    indexmat(1) = 0
    do i = 1, e  !!global part
        add = 0
        add1 = 0
        mat = 0
        if(i <= e1)then
            add = 4
            if(n(globalnum(i,2),1) <= 3.01d0 .and. n(globalnum(i,3),2) <= 3.01d0)then  
                do j = e1+1, e !!含むローカル要素
                    check = 0  !!要素かぶりチェッカー
                    quad(1,1) = n(globalnum(j,1),1)
                    quad(2,1) = n(globalnum(j,1),1)
                    quad(3,2) = n(globalnum(j,1),2)
                    quad(1,2) = n(globalnum(j,1),2)
                    do k = 1, 4 !!長方形の頂点作成
                        if(n(globalnum(j,k),1) >= quad(2,1))then
                            quad(2,1) = n(globalnum(j,k),1)
                            quad(3,1) = n(globalnum(j,k),1)
                        end if
                        if(n(globalnum(j,k),1) <= quad(1,1))then
                            quad(1,1) = n(globalnum(j,k),1)
                            quad(4,1) = n(globalnum(j,k),1)
                        end if
                        if(n(globalnum(j,k),2) >= quad(3,2))then
                            quad(3,2) = n(globalnum(j,k),2)
                            quad(4,2) = n(globalnum(j,k),2)
                        end if
                        if(n(globalnum(j,k),2) <= quad(1,2))then
                            quad(1,2) = n(globalnum(j,k),2)
                            quad(2,2) = n(globalnum(j,k),2)
                        end if
                    end do
                    do l = 1, 4  !!長方形内の節点ループ
                        if(check == 1)then
                            cycle
                        end if
                        call find_elem(quad(l,1),quad(l,2),a,b,c1,n,globalnum,cordx,cordy,en)
                        if(en == i)then
                            do m = 1, 4
                                check2 = 0  !!節点かぶりチェッカー
                                do o = 1, pn2
                                    if(globalnum(j,m) == mat(o))then
                                        check2 = 1
                                    end if
                                end do
                                if(check2 == 0)then
                                    add1 = add1 + 1
                                    mat(add1) = globalnum(j,m)
                                end if
                            end do
                            check = 1
                        end if
                    end do
                end do
            end if
            add = add + add1
        end if
        if(i > e1)then !!local part
            enn1 = 0
            enmax = 0
            add = 4
            quad(1,1) = n(globalnum(i,1),1)
            quad(2,1) = n(globalnum(i,1),1)
            quad(3,2) = n(globalnum(i,1),2)
            quad(1,2) = n(globalnum(i,1),2)
            do k = 1, 4 !!長方形の頂点作成
                if(n(globalnum(i,k),1) >= quad(2,1))then
                    quad(2,1) = n(globalnum(i,k),1)
                    quad(3,1) = n(globalnum(i,k),1)
                end if
                if(n(globalnum(i,k),1) <= quad(1,1))then
                    quad(1,1) = n(globalnum(i,k),1)
                    quad(4,1) = n(globalnum(i,k),1)
                end if
                if(n(globalnum(i,k),2) >= quad(3,2))then
                    quad(3,2) = n(globalnum(i,k),2)
                    quad(4,2) = n(globalnum(i,k),2)
                end if
                if(n(globalnum(i,k),2) <= quad(1,2))then
                    quad(1,2) = n(globalnum(i,k),2)
                    quad(2,2) = n(globalnum(i,k),2)
                end if
            end do
            do j = 1, 4
                call find_elem(quad(j,1),quad(j,2),a,b,c1,n,globalnum,cordx,cordy,en)
                if(en == enmax(1) .or. en == enmax(2) .or. en == enmax(3) .or. en == enmax(4))then
                    cycle
                end if
                enmax(j) = en
                enn1 = enn1 + 1
            end do
            if(enn1 == 1)then
                add = add + 4
            else if(enn1 == 2)then
                add = add + 6
            else if(enn1 == 3)then
                add = add + 8
            else if(enn1 == 4)then
                add = add + 9
            end if
        end if
        indexmat(i+1) = indexmat(i) + add
    end do
    end subroutine

    subroutine item(e,e1,a,b,c1,n,globalnum,indexmat,itemmat)
    implicit none
    integer,allocatable::globalnum(:,:)
    integer,pointer::indexmat(:),itemmat(:)
    integer e,pn,ep,i,j,c1,k,e1,e2,add,en,l,m,check,check2,enmax(4)
    real(8) a,b,cordx,cordy,quad(4,2)
    real(8), allocatable ::n(:,:)
    allocate(itemmat(indexmat(e+1)),source = 0)
    do i = 1, e
        if(i <= e1)then  !!global part
            add = 4
            do j = 1, add
                itemmat(indexmat(i)+j) = globalnum(i,j)
            end do
            if(n(globalnum(i,2),1) <= 3.01d0 .and. n(globalnum(i,3),2) <= 3.01d0)then  
                do j = e1+1, e !!含むローカル要素
                    check = 0  !!要素かぶりチェッカー
                    quad(1,1) = n(globalnum(j,1),1)
                    quad(2,1) = n(globalnum(j,1),1)
                    quad(3,2) = n(globalnum(j,1),2)
                    quad(1,2) = n(globalnum(j,1),2)
                    do k = 1, 4 !!長方形の頂点作成
                        if(n(globalnum(j,k),1) >= quad(2,1))then
                            quad(2,1) = n(globalnum(j,k),1)
                            quad(3,1) = quad(2,1)
                        end if
                        if(n(globalnum(j,k),1) <= quad(1,1))then
                            quad(1,1) = n(globalnum(j,k),1)
                            quad(4,1) = n(globalnum(j,k),1)
                        end if
                        if(n(globalnum(j,k),2) >= quad(3,2))then
                            quad(3,2) = n(globalnum(j,k),2)
                            quad(4,2) = quad(3,2)
                        end if
                        if(n(globalnum(j,k),2) <= quad(1,2))then
                            quad(1,2) = n(globalnum(j,k),2)
                            quad(2,2) = quad(1,2)
                        end if
                    end do
                    do l = 1, 4  !!長方形内の節点ループ
                        if(check == 1)then
                            cycle
                        end if
                        call find_elem(quad(l,1),quad(l,2),a,b,c1,n,globalnum,cordx,cordy,en)
                        if(en == i)then
                            do m = 1, 4
                                check2 = 0  !!節点かぶりチェッカー
                                do o = indexmat(i)+1, indexmat(i)+add
                                    if(globalnum(j,m) == itemmat(o))then
                                        check2 = 1
                                    end if
                                end do
                                if(check2 == 0)then
                                    add = add + 1
                                    itemmat(indexmat(i)+add) = globalnum(j,m)
                                end if
                            end do
                            check = 1
                        end if
                    end do
                end do
            end if
        end if
        if(i > e1)then  !!local part
            enmax = 0
            add = 4
            do j = 1, add
                itemmat(indexmat(i)+j) = globalnum(i,j)
            end do
            quad(1,1) = n(globalnum(i,1),1)
            quad(2,1) = n(globalnum(i,1),1)
            quad(3,2) = n(globalnum(i,1),2)
            quad(1,2) = n(globalnum(i,1),2)
            do k = 1, 4 !!長方形の頂点作成
                if(n(globalnum(i,k),1) >= quad(2,1))then
                    quad(2,1) = n(globalnum(i,k),1)
                    quad(3,1) = n(globalnum(i,k),1)
                end if
                if(n(globalnum(i,k),1) <= quad(1,1))then
                    quad(1,1) = n(globalnum(i,k),1)
                    quad(4,1) = n(globalnum(i,k),1)
                end if
                if(n(globalnum(i,k),2) >= quad(3,2))then
                    quad(3,2) = n(globalnum(i,k),2)
                    quad(4,2) = n(globalnum(i,k),2)
                end if
                if(n(globalnum(i,k),2) <= quad(1,2))then
                    quad(1,2) = n(globalnum(i,k),2)
                    quad(2,2) = n(globalnum(i,k),2)
                end if
            end do
            
            do l = 1, 4  !!長方形内の節点ループ
                call find_elem(quad(l,1),quad(l,2),a,b,c1,n,globalnum,cordx,cordy,en)!
                if(en == enmax(1) .or. en == enmax(2) .or. en == enmax(3) .or. en == enmax(4))then  !!要素かぶりチェッカー
                    cycle
                end if
                enmax(l) = en
                do m = 1, 4
                    check = 0  !!節点かぶりチェッカー
                    do o = 1, add
                        if(globalnum(en,m) == itemmat(indexmat(i)+o))then
                            check = 1
                        end if
                    end do
                    if(check == 0)then
                        add = add + 1
                        itemmat(indexmat(i)+add) = globalnum(en,m) 
                    end if
                end do
            end do           
        end if       
    end do
    end subroutine
    end module
