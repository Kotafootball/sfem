program main
use module
use module2
use module3
use mod_monolis
implicit none

integer i,j,l,aa, Nn, bc0, f1, e, ep, ld0, g, f2, pn,e1,e2,ep1,ep2,pn1,pn2,pn3,ep3,e3,c1,c2,n11,n22,en,en1,spn,se,iii
integer,allocatable::globalnum(:,:),bc(:,:),ld(:,:),connectivity(:),globalnumt(:,:),globalnum1(:,:),&
globalnum2(:,:),connectivity1(:),connectivity2(:),globalnum3(:,:),surfacenum(:,:)
integer,pointer::indexmat(:),itemmat(:)
real(8), allocatable::u(:), f(:),n(:,:),bc1(:),ldf(:),trac_local(:,:),trac_global(:,:),diff(:,:)&
,n1(:,:),n2(:,:), KGL(:,:),n3(:,:),el22(:),eps(:,:),sgm(:,:),load(:,:),diff_e(:,:)
real(8) ::bm(3,8),d(3,3),jm(2,2),jmi(2,2),Ke(8,8),eps1(3),sgm1(3),trac1(2),dif1(2),dif2(2),dif3(2),dif4(2)
real(8) y, p,t1,t2,y1,p1,y2,p2,a,b,r1,r2,el2,gzi,ita,r,nvx,nvy,xx,yy,sgmx,sgmy,epsx,epsy,ux,uy,uthx,uthy,b1,alpha
type(monolis_structure) :: K ! monolis 構造体 Ax = b の疎⾏列相当

call cpu_time(t1)

call read_file1(y1,p1,y2,p2,a,b,c1,c2) 
call read_file2(pn1, n1)
call read_file3(e1, ep1, globalnum1) 
call read_file22(pn2, n2)
call read_file33(e2, ep2, globalnum2)
call read_file222(pn3, n3)
call read_file333(e3, ep3, globalnum3)
pn = pn1 + pn2
e = e1 + e2
ep = ep1
allocate(n(pn,2),globalnum(e,ep))
do i = 1, pn1
  do j = 1, 2
    n(i,j) = n1(i,j)
  enddo
enddo
do i = pn1+1, pn
  do j = 1,2
    n(i,j) = n2(i-pn1,j)
  enddo
enddo
do i = 1, e1
  do j = 1, ep
    globalnum(i,j) = globalnum1(i,j)
  enddo
enddo
do i = e1+1, e
  do j = 1,ep
    globalnum(i,j) = globalnum2(i-e1,j) + pn1
  enddo
enddo
call makebc(b,n,pn,pn1,y1,y2,p1,p2,globalnum)

Nn = pn * 2
allocate(u(Nn), f(Nn), source = 0.0d0)
allocate(connectivity(ep), source = 0)
allocate(globalnumt(ep,e), source = 0)
allocate(KGL(8,8), source =0.0d0)
allocate(eps(3,e),sgm(3,e),source = 0.0d0)
n11 = 4
n22 = 4
allocate(connectivity1(n11),connectivity2(n22),source = 0)
call make_surface(pn,pn1,pn2,n,surfacenum,spn,se)
allocate(trac_local(se*2,2),trac_global(se*2,2), diff(se*2,2),diff_e(se,2),load(pn,2),source = 0.0d0)

!do iii = 1, 2
call read_file4(bc0 ,f1, bc, bc1)
call read_file5(ld0,f2,ld,ldf)
!monolis
call monolis_global_initialize() ! monolis の全体初期化
call monolis_initialize(K, "./") ! monolis の初期化
!> この部分で疎⾏列 A の構成、求解が可能
do i = 1, e
  do j = 1, ep
    globalnumt(j,i) = globalnum(i,j)
  end do
end do

call index(a,b,c1,e1,e,pn2,n,globalnum,indexmat)
call item(e,e1,a,b,c1,n,globalnum,indexmat,itemmat)
call monolis_get_nonzero_pattern_by_connectivity(K, pn, f1, e, indexmat, itemmat) ! 疎⾏列パターンの決定
do i = 1,pn1
  do j = pn+1,pn2
    if(n(i,1) == n(j,1) .and. n(i,2) == n(j,2))then
      write(*,*)i,jm
    end if
  end do
end do

aa = 50
do i = 1, e   !!!global+local
  r1 = sqrt(n(globalnum(i,1),1) ** 2.0d0 + n(globalnum(i,1),2) ** 2.0d0)
  r2 = sqrt(n(globalnum(i,3),1) ** 2.0d0 + n(globalnum(i,3),2) ** 2.0d0)
  if(i <= e1)then
    if(r1 < 3.00d0 .and. r2 > 3.00d0)then
      call KG(aa,i,globalnum, n, y,p,y1,p1,y2,p2,d,jm,jmi,bm,Ke)  !境界部分のグローバル要素剛性行列
    else
      call Kesub(i,globalnum, n, y,p,y1,p1,y2,p2,d,jm,jmi,bm,Ke)  !その他のグローバル要素剛性行列
    end if
    do j = 1, ep
      connectivity(j) = globalnum(i,j) 
    end do
    call monolis_add_matrix_to_sparse_matrix(K, ep, connectivity, Ke)! ⾏列成分の⾜し込み
  else if(i > e1)then
    call Kesub(i,globalnum, n, y,p,y1,p1,y2,p2,d,jm,jmi,bm,Ke)  !ローカル要素剛性行列
    do j = 1, ep
      connectivity(j) = globalnum(i,j) 
    end do
    call monolis_add_matrix_to_sparse_matrix(K, ep, connectivity, Ke)! ⾏列成分の⾜し込み
    
    do o = 1, aa
      do q = 1, aa
        do j = 1, ep
          call KGLmat(aa,o,q,j,i,y,p,y1,p1,globalnum,n,a,b,c1,en1,KGL)!!KGL
          do l = 1, n11
            connectivity1(l) = globalnum(i,l)
          end do
          do l = 1, n22
            connectivity2(l) = globalnum(en1,l) !!積分点が含まれるグローバル要素番号がわかればよい
          end do
          call monolis_add_matrix_to_sparse_matrix_offdiag(K, n11, n22, connectivity1, connectivity2, transpose(KGL))! 積分点ごとに⾏列成分の⾜し込み
          call monolis_add_matrix_to_sparse_matrix_offdiag(K, n22, n11, connectivity2, connectivity1, KGL)! 積分点ごとに⾏列成分の⾜し込み
        end do
      end do
    end do
  end if
end do

do i = 1, bc0
  call monolis_set_Dirichlet_bc(K, f, bc(i,1), bc(i,2), bc1(i)) ! Dirichlet 境界条件の付与
end do

!do i = 1, ld0
!  if(ld(i,2) == 1)then
!    f(ld(i,1) * 2 - 1) = f(ld(i,1) * 2 - 1) + ldf(i)  !Neumann境界条件の付与
!  else if(ld(i,2) == 2)then
!    f(ld(i,1) * 2) = f(ld(i,1) * 2) + ldf(i)
!  end if
!end do

call monolis_param_set_maxiter(K, 100000) !> 反復法の最大反復回数を 10000 回に設定
call monolis_param_set_precond(K, monolis_prec_SOR)
call monolis_param_set_tol(K, 1.0d-7)
call monolis_param_set_is_debug(K, .true.)
call monolis_solve(K,f,u) ! 求解 --> ** monolis solver: CG, prec: Diag

call monolis_finalize(K) ! monolis の終了処理
call monolis_global_finalize() ! monolis の全体終了処理
!monolis finish
call add_u(pn,pn1,n,a,b,c1,globalnum,u)
aa = 50 !!誤差評価の数値積分の分割数
call sfemerror(aa,y1,y2,p1,p2,b,n,e3,e1,e2,globalnum,u,el2,el22,a,n3,globalnum3,c1)
open(31, file = "dispinclusion.dat", status = "replace")
    do i = 1, Nn
    write(31,*) u(i)
    enddo
close(31)


do i = 1, se  !線要素ループ
  do j = e1+1, e  !ローカル要素ループ
    if(globalnum(j,2) == surfacenum(i,1) .and. globalnum(j,3) == surfacenum(i,2))then
      gzi = 1.0d0
      do o = 1, 2  !積分点ループ
        if(o == 1)then
          ita = -0.577350269189626d0
        else
          ita = 0.577350269189626d0
        end if
        call find_n(n(surfacenum(i,1),1),n(surfacenum(i,1),2),n(surfacenum(i,2),1),n(surfacenum(i,2),2),nvx,nvy)
        call stress(j,gzi,ita,y1,y2,p1,p2,globalnum,n,u,eps1,sgm1,e1)
        call trac(nvx,nvy,sgm1,trac1)
        !write(*,*)trac1(1),trac1(2)
        write(*,*)sgm1(1),sgm1(2),sgm1(3)
        call sekibunten(j,globalnum,n,gzi,ita,xx,yy)
        call stresscheck(b,xx,yy,y1,y2,p1,p2,epsx,epsy,sgmx,sgmy)
        write(*,*)sgmx,sgmy,"理論解"
        do l = 1, 2
          if(o == 1)then
            trac_local(i*2-1,l) = trac1(l)!ローカルトラクションを求めて保存する
          else
            trac_local(i*2,l) = trac1(l)
          end if
        end do

        call sekibunten(j,globalnum,n,gzi,ita,xx,yy)
        call find_elem(xx,yy,a,b,c1,n,globalnum,gzi,ita,en)
        call stress(en,gzi,ita,y1,y2,p1,p2,globalnum,n,u,eps1,sgm1,e1)
        nvx = -nvx
        nvy = -nvy
        call trac(nvx,nvy,sgm1,trac1)
        do l = 1, 2
          if(o == 1)then
            trac_global(i*2-1,l) = trac1(l)!グローバルトラクションを求めて保存する
          else
            trac_global(i*2,l) = trac1(l)
          end if
        end do

        if(o == 1)then
          l = i * 2 - 1 !積分点の番号
        else
          l = i * 2
        end if
        call diff_trac(l,trac_local,trac_global,diff) !積分点ごとのトラクションの差を計算
        !write(*,*)diff(l,1),diff(l,2),"差"
        if(o == 1)then
          ita = -0.577350269189626d0
        else
          ita = 0.577350269189626d0
        end if
        gzi = 1.0d0
        call sekibun(j,l,gzi,ita,diff,load,globalnum,n) !線要素の節点に調整力を分配
      end do
    end if
  end do
end do

do i = pn1+1, pn !グローバル節点への分配
  if(load(i,1) /= 0.0d0 .or. load(i,2) /= 0.0d0)then
    call find_elem(n(i,1),n(i,2),a,b,c1,n,globalnum,gzi,ita,en)
    call tasikomi(i,en,gzi,ita,globalnum,load)
  end if
end do

call makeload(pn,load)

call additional(e,y1,y2,p1,p2,globalnum,n,u,eps,sgm)
call print_text2(g,ep,e3,pn3,globalnum3,n3,el22)
call print_text(g,ep,e,pn,pn1,globalnum,n,u,sgm)
write(*,*) " "
write(*,*) "error", el2 
write(*,*) "e1",e1,"e2",e2,"e",e
write(*,*) "pn",pn,"dof",pn*f1, c1
call cpu_time(t2)
write(*,*) "time", t2 - t1 ,"sec"

!end do !iii
end program