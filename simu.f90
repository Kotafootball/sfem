program main
use module
use module2
use mod_monolis
implicit none
integer i,j,l,aa, Nn, bc0, f1, e, ep, ld0, g, f2, pn,e1,e2,ep1,ep2,pn1,pn2,pn3,ep3,e3,c1,c2,n11,n22,en,en1,spn,se
integer,allocatable::globalnum(:,:),bc(:,:),ld(:,:),connectivity(:),globalnumt(:,:),globalnum1(:,:),&
globalnum2(:,:),connectivity1(:),connectivity2(:),globalnum3(:,:),surfacenum(:,:)
integer,pointer::indexmat(:),itemmat(:)
real(8), allocatable::u(:), f(:),n(:,:),bc1(:),ldf(:),trac_local(:,:),trac_global(:,:),diff(:,:)&
,n1(:,:),n2(:,:), KGL(:,:),n3(:,:),el22(:),eps(:,:),sgm(:,:),load(:,:),diff_e(:,:)
real(8) ::bm(3,8),d(3,3),jm(2,2),jmi(2,2),Ke(8,8),eps1(3),sgm1(3),trac1(2),dif1(2),dif2(2),dif3(2),dif4(2)
real(8) y, p,t1,t2,y1,p1,y2,p2,a,b,r1,r2,el2,gzi,ita,r,nvx,nvy,xx,yy,sgmx,sgmy,epsx,epsy,ux,uy,uthx,uthy,b1,alpha
type(monolis_structure) :: K ! monolis 構造体 Ax = b の疎⾏列相当

y = 10.00d0
p = 0.25
pn = 6
e = 2
ep = 4
Nn = pn * 2
allocate(globalnum(e,4),globalnumt(4,e),n(pn,2),connectivity(ep))
allocate(u(Nn), f(Nn), source = 0.0d0)
n(1,1) = 0.0d0
n(1,2) = 0.0d0
n(2,1) = 1.0d0
n(2,2) = 0.0d0
n(3,1) = 2.0d0
n(3,2) = 0.0d0
n(4,1) = 0.0d0
n(4,2) = 1.0d0
n(5,1) = 1.0d0
n(5,2) = 1.0d0
n(6,1) = 2.0d0
n(6,2) = 1.0d0

globalnum(1,1) = 1
globalnum(1,2) = 2
globalnum(1,3) = 5
globalnum(1,4) = 4
globalnum(2,1) = 2
globalnum(2,2) = 3
globalnum(2,3) = 6
globalnum(2,4) = 5

call makebc22()
call read_file4(bc0 ,f1, bc, bc1)
!monolis
call monolis_global_initialize() ! monolis の全体初期化
call monolis_initialize(K, "./") ! monolis の初期化
!> この部分で疎⾏列 A の構成、求解が可能
do i = 1, e
  do j = 1, ep
    globalnumt(j,i) = globalnum(i,j)
  end do
end do
call monolis_get_nonzero_pattern(K, pn, ep, f1, e, globalnumt) ! 疎⾏列パターンの決定

do i = 1, e   !!!global+local
  call Kesub22(i,globalnum, n, y,p,d,jm,jmi,bm,Ke)  !境界部分のグローバル要素剛性行列
    do j = 1, ep
      connectivity(j) = globalnum(i,j) 
    end do
    call monolis_add_matrix_to_sparse_matrix(K, ep, connectivity, Ke)! ⾏列成分の⾜し込み
end do
do i = 1, bc0
  call monolis_set_Dirichlet_bc(K, f, bc(i,1), bc(i,2), bc1(i)) ! Dirichlet 境界条件の付与
end do

call monolis_param_set_maxiter(K, 100000) !> 反復法の最大反復回数を 10000 回に設定
call monolis_param_set_precond(K, monolis_prec_SOR)
call monolis_param_set_tol(K, 1.0d-7)
call monolis_param_set_is_debug(K, .true.)
call monolis_solve(K,f,u) ! 求解 --> ** monolis solver: CG, prec: Diag

call monolis_finalize(K) ! monolis の終了処理
call monolis_global_finalize() ! monolis の全体終了処理
!monolis finish


do i = 1, pn
write(*,*)u(i*2-1),u(i*2)
end do
allocate(eps(3,e),sgm(3,e))
gzi = 1.0d0
ita = 0.0d0
do i = 1, e
    call stress22(i,gzi,ita,y,p,globalnum,n,u,eps,sgm)
    write(*,*)sgm(1,i),sgm(2,i),sgm(3,i)
end do

contains
subroutine makebc22()
implicit none
integer bc0
open(32, file = "bc.dat", status = "replace")
   bc0 = 4
    write(32,*) bc0, 2
    write(32,*) 1, 1, 0.0d0
    write(32,*) 4, 1, 0.0d0
    write(32,*) 3, 1, 1.0d0
    write(32,*) 6, 1, 1.0d0
close (32)
end subroutine

subroutine Kesub22(j,globalnum, n, y,p,d,jm,jmi,bm,Ke)
    implicit none
    integer,allocatable::globalnum(:,:)
    integer j,pn,ep,i,k,l,o,aa
    real(8):: a, c, det, p, y,xx,yy,r,nn1,nn2,nn3,nn4
    real(8):: bdbdetp(8,8), bdb(8,8), db(3,8), jm(2,2), bt(8,3) = 0.0d0,&
             d(3,3), jmi(2,2), nxy(4,2) = 0.0d0, bm(3,8), Ke(8,8)
    real(8), allocatable ::n(:,:)
   
    Ke = 0.0d0
        do ii = 1, 4
            bdbdetp = 0.0d0
            bdb = 0.0d0
            db = 0.0d0
            call gziita(ii,c,a,nn1,nn2,nn3,nn4)
            call jmat(jm,c,a,n,globalnum,j)
            call Bmat(bm,nxy,jmi,jm,c,a)
            call Dmat(d,y,p)
            do o = 1, 3
               do q = 1, 8
                  bt(q,o) = bm(o,q) !btを定義
               end do
            end do
         
            do o = 1, 3
               do q = 1, 8
                  do w = 1, 3
                     db(o,q) = db(o,q) + d(o,w) * bm(w,q) !dbを計算
                  end do
               end do
            end do
         
            do o = 1, 8
               do q = 1, 8
                  do w = 1, 3
                     bdb(o,q) = bdb(o,q) + bt(o,w) * db(w,q) !bdbを計算
                  end do
               end do
            end do
 
            det = (jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)) !det計算

            if(det < 0) then
               write(*,*) "det<0"
               stop 
            end if
              
            do o = 1, 8
               do q = 1, 8
                  bdbdetp(q,o) = bdb(q,o) * det !bdbdetp計算
               end do
            end do

            do o = 1,8
               do l = 1,8
                  Ke(o,l) = Ke(o,l) + bdbdetp(o,l)
               end do
            end do
        end do
    end subroutine

subroutine stress22(e,c,a,y,p,globalnum,n,u,eps,sgm)
implicit none
integer i, j, l, e,e1
integer,allocatable::globalnum(:,:)
real(8), allocatable::u(:),n(:,:)
real(8) ::bm(3,8) = 0.0d0,d(3,3),jm(2,2),jmi(2,2),nxy(4,2),eps(3),sgm(3)
real(8) y, p,a,c

      eps = 0.0d0
      sgm = 0.0d0

      do i = 1, 3
         call jmat(jm,c,a,n,globalnum,e)
         call Bmat(bm,nxy,jmi,jm,c,a)
         do l = 1, 8
            if (mod(l,2) == 1)then
               eps(i) = eps(i) + bm(i,l) * u(globalnum(e,(l + 1) / 2) * 2 - 1)
            else if (mod(l,2) == 0) then
               eps(i) = eps(i) + bm(i,l) * u(globalnum(e,l / 2) * 2)
            endif
         enddo
      enddo
      !write(*,*)eps(1),eps(2),eps(3)

      do i = 1, 3
         call Dmat(d,y,p)
         do l = 1, 3
            sgm(i) = sgm(i) + d(i,l) * eps(l)
         enddo
      enddo
end subroutine

end program