module module
  implicit none
  integer ii, o, q, w

contains
  
  subroutine read_file1(y1,p1,y2,p2,a1,b1,c1,c2)
    implicit none
    integer c1,c2
    real(8) y1, p1, y2, p2, a1, b1
    open(10, file="input.dat", status="old")
    read (10, *) y1, p1
    read (10, *) y2,p2
    read(10,*) a1, b1
    read(10,*)c1,c2
    close (10)
  end subroutine read_file1
  
  subroutine read_file2(pn,n)
    implicit none
    integer pn
    real(8),allocatable::n(:,:)
    open(11, file="nodeglobal.dat", status="old")
    read (11, *) pn
    allocate(n(pn,2))
    do ii = 1, pn
       read(11,*) n(ii,1), n(ii,2)
    end do
    close (11)
  end subroutine read_file2

  subroutine read_file3(e,ep,globalnum)
    implicit none
    integer e, ep
    integer,allocatable::globalnum(:,:)
    open(12, file="elemglobal.dat", status="old")
    read (12, *)e, ep
    allocate (globalnum(e,ep), source = 0)
    do ii = 1, e
       read(12,*)globalnum(ii,1), globalnum(ii,2), globalnum(ii,3),globalnum(ii,4)
    end do
    close (12)
  end subroutine read_file3


subroutine read_file22(pn,n)
    implicit none
    integer pn
    real(8),allocatable::n(:,:)
    open(11, file="nodelocal.dat", status="old")
    read (11, *) pn
    allocate(n(pn,2))
    do ii = 1, pn
       read(11,*) n(ii,1), n(ii,2)
    end do
    close (11)
  end subroutine read_file22

  subroutine read_file33(e,ep,globalnum)
    implicit none
    integer e, ep
    integer,allocatable::globalnum(:,:)
    open(12, file="elemlocal.dat", status="old")
    read (12, *)e, ep
    allocate (globalnum(e,ep), source = 0)
    do ii = 1, e
       read(12,*)globalnum(ii,1), globalnum(ii,2), globalnum(ii,3),globalnum(ii,4)
    end do
    close (12)
  end subroutine read_file33

  subroutine read_file222(pn,n)
    implicit none
    integer pn
    real(8),allocatable::n(:,:)
    open(11, file="nodeerror.dat", status="old")
    read (11, *) pn
    allocate(n(pn,2))
    do ii = 1, pn
       read(11,*) n(ii,1), n(ii,2)
    end do
    close (11)
  end subroutine read_file222

   subroutine read_file333(e,ep,globalnum)
    implicit none
    integer e, ep
    integer,allocatable::globalnum(:,:)
    open(12, file="elemerror.dat", status="old")
    read (12, *)e, ep
    allocate (globalnum(e,ep), source = 0)
    do ii = 1, e
       read(12,*)globalnum(ii,1), globalnum(ii,2), globalnum(ii,3),globalnum(ii,4)
    end do
    close (12)
   end subroutine read_file333

  subroutine read_file4(bc0,f1,bc,bc1)
    implicit none
    integer bc0, f1
    integer,allocatable::bc(:,:)
    real(8),allocatable::bc1(:)
    open(13, file="bc.dat", status="old")
    read (13, *) bc0, f1
    allocate(bc(bc0,2), source = 0)
    allocate(bc1(bc0), source = 0.0d0)
    do ii = 1, bc0
       read (13, *)bc(ii,1), bc(ii,2), bc1(ii)
    end do
    close (13)
  end subroutine read_file4

  subroutine read_file5(ld0,f2,ld,ldf)
    implicit none
    integer ld0, f2
    integer,allocatable::ld(:,:)
    real(8),allocatable::ldf(:)
    open(14, file="load.dat", status="old")
    read (14, *)ld0, f2
    allocate(ld(ld0,2), source = 0)
    allocate(ldf(ld0), source = 0.0d0)
    do ii = 1, ld0
       read(14, *) ld(ii,1), ld(ii,2), ldf(ii)  !!節点番号、自由度方向、節点力値
    end do
    close (14)
  end subroutine read_file5

  subroutine read_file6(u,pn,f1)
    implicit none
    integer pn,f1,Nn
    real(8),allocatable:: u(:)

    Nn = pn * f1
    allocate(u(Nn), source = 0.0d0)
    open(15, file="disp.dat", status="old")
    do ii = 1, Nn
       read(15,*)u(ii)
    end do
    close (15)
  end subroutine read_file6

  subroutine print_text(g,ep,e,pn,pn1,globalnum,n,u,sgm)
    implicit none
    integer g, ep, e, se
    integer pn,pn1
    integer,allocatable::globalnum(:,:)
    real(8),allocatable::n(:,:),u(:),eps(:,:),sgm(:,:)!,el2(:)
    g = (1 + ep) * e
    open(30, file = "sfem.vtk", status = "replace")
    write(30,"(a)")"# vtk DataFile Version 4.1"
    write(30,"(a)")"this is command line"
    write(30,"(a)")"ASCII"
    write(30,"(a)")"DATASET UNSTRUCTURED_GRID"
    write(30,*)" "
    write(30,"(a,i8,1x,a)")trim("POINTS"), pn, trim("float")
    do ii =1, pn1
       write(30,"(f15.5,1x,f15.5,1x,f15.5)") n(ii,1),n(ii,2), 0.0d0
    end do
    do ii =pn1 + 1, pn
       write(30,"(f15.5,1x,f15.5,1x,f15.5)") n(ii,1),n(ii,2), 0.1d0
    end do
    write(30,*)" "
    write(30,"(a,i8,i8)")trim("CELLS"),e,g
    do ii = 1, e
       write(30,*)ep,globalnum(ii,1) - 1, globalnum(ii,2) - 1, globalnum(ii,3) - 1, globalnum(ii,4) - 1
    end do
    write(30,*)" "
    write(30,"(a,i8)")trim("CELL_TYPES"),e
    do ii = 1, e
       write(30,*)"9"
    end do

    write(30,*)" "
    write(30,"(a,1x,i6)")trim("POINT_DATA"),pn
    write(30,"(a,1x,a,1x,a)")trim("VECTORS"),trim("Displacement"),trim("float")
    do ii = 1 , pn
      write(30,*)u(2 * ii - 1), u(2 * ii), 0.0d0
    enddo

   write(30,*)" "
   write(30,"(a,i6)")trim("CELL_DATA"),e
   write(30,"(a,1x,a,1x,a)")trim("VECTORS"),trim("cell_vector_data"),trim("float")
   do ii = 1,e
     write(30,*)sgm(1,ii) ,sgm(2,ii), sgm(3,ii)
   enddo
  
    close (30)
  end subroutine print_text

  subroutine print_text2(g,ep,e,pn,globalnum,n,el2)
    implicit none
    integer g, ep, e, i, pn
    integer,allocatable::globalnum(:,:)
    real(8),allocatable::n(:,:),u(:),eps(:,:),sgm(:,:),el2(:)
    g = (1 + ep) * e
    open(30, file = "el2.vtk", status = "replace")
    write(30,"(a)")"# vtk DataFile Version 4.1"
    write(30,"(a)")"this is command line"
    write(30,"(a)")"ASCII"
    write(30,"(a)")"DATASET UNSTRUCTURED_GRID"
    write(30,*)" "
    write(30,"(a,i8,1x,a)")trim("POINTS"), pn, trim("float")
    do i =1, pn
       write(30,"(f15.5,1x,f15.5,1x,f15.5)") n(i,1),n(i,2), 0.0
    end do
    write(30,*)" "
    write(30,"(a,i8,i8)")trim("CELLS"),e,g
    do i = 1, e
       write(30,*)ep,globalnum(i,1) - 1, globalnum(i,2) - 1, globalnum(i,3) - 1, globalnum(i,4) - 1
    end do
    write(30,*)" "
    write(30,"(a,i8)")trim("CELL_TYPES"),e
    do i = 1, e
       write(30,*)"9"
    end do

    write(30,*)" "
    write(30,"(a,1x,i6)")trim("CELL_DATA"),e
    write(30,"(a,1x,a,1x,a)")trim("SCALARS"),trim("error-map"),trim("double")
    write(30,"(a,1x,a)")trim("LOOKUP_TABLE"),trim("default")
    do i = 1 , e
      write(30,"(f15.12)")el2(i)
    enddo

    close (30)
  end subroutine print_text2

  subroutine gziita(i,c,a,nn1,nn2,nn3,nn4)
   implicit none
   integer i
   real(8) a,c,nn1,nn2,nn3,nn4
      if (i == 1)then
         a = -0.577350269189626d0
         c = -0.577350269189626d0
      else if (i == 2)then
         a = -0.577350269189626d0
         c = 0.577350269189626d0
      else if (i == 3)then
         a = 0.577350269189626d0
         c = 0.577350269189626d0
      else if (i == 4)then
         a = 0.577350269189626d0
         c = -0.577350269189626d0
      end if
      nn1 = (1.0d0-c)*(1.0d0-a)
      nn2 = (1.0d0+c)*(1.0d0-a)
      nn3 = (1.0d0+c)*(1.0d0+a)
      nn4 = (1.0d0-c)*(1.0d0+a)
   end subroutine

   subroutine Dmat(d,y,p) !平面ひずみ状態
   implicit none
   real(8) y,p,d(3,3)
      d(1,1) = y / (1.0d0 + p) / (1.0d0 - 2.0d0*p)*(1.0d0-p)
      d(1,2) = y / (1.0d0 + p) * p / (1.0d0 -2.0d0* p)
      d(1,3) = 0.0d0
      d(2,1) = y / (1.0d0 + p) * p / (1.0d0 - 2.0d0*p)
      d(2,2) = y / (1.0d0 + p) / (1.0d0 -2.0d0* p)*(1.0d0-p)
      d(2,3) = 0.0d0
      d(3,1) = 0.0d0
      d(3,2) = 0.0d0
      d(3,3) = 1.0d0 / 2.0d0 * y / (1.0d0 + p)
   end subroutine

   subroutine Dmat2(d,y,p) !平面応力状態
   implicit none
   real(8) y,p,d(3,3)
      d(1,1) = y / (1.0d0 + p) / (1.0d0 - p)
      d(1,2) = y / (1.0d0 + p) * p / (1.0d0 - p)
      d(1,3) = 0.0d0
      d(2,1) = y / (1.0d0 + p) * p / (1.0d0 - p)
      d(2,2) = y / (1.0d0 + p) / (1.0d0 - p)
      d(2,3) = 0.0d0
      d(3,1) = 0.0d0
      d(3,2) = 0.0d0
      d(3,3) = 1.0d0 / 2.0d0 * y / (1.0d0 + p)
   end subroutine

   subroutine jmat(jm,c,a,n,globalnum,j)
   implicit none
   integer j
   real(8) jm(2,2),c,a
   integer,allocatable::globalnum(:,:)
   real(8),allocatable::n(:,:)
      jm(1,1) = ((-1.0d0 + a) * n(globalnum(j,1),1) +(1.0d0 - a) * n(globalnum(j,2),1) +&
               (1.0d0 + a) * n(globalnum(j,3),1) + (-1.0d0 - a) * n(globalnum(j,4),1)) / 4.0d0
      jm(2,1) = ((-1.0d0 + c) * n(globalnum(j,1),1) +(-1.0d0 - c) * n(globalnum(j,2),1) +&
               (1.0d0 + c) * n(globalnum(j,3),1) + (1.0d0 - c) * n(globalnum(j,4),1)) / 4.0d0
      jm(1,2) = ((-1.0d0 + a) * n(globalnum(j,1),2) +(1.0d0 - a) * n(globalnum(j,2),2) +&
               (1.0d0 + a) * n(globalnum(j,3),2) + (-1.0d0 - a) * n(globalnum(j,4),2)) / 4.0d0
      jm(2,2) = ((-1.0d0 + c) * n(globalnum(j,1),2) +(-1.0d0 - c) * n(globalnum(j,2),2) +&
               (1.0d0 + c) * n(globalnum(j,3),2) + (1.0d0 - c) * n(globalnum(j,4),2)) / 4.0d0
   end subroutine

   subroutine Bmat(bm,nxy,jmi,jm,c,a)
   implicit none
   real(8) jm(2,2),jmi(2,2),nxy(4,2),bm(3,8),c,a
      jmi(1,1) = jm(2,2) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(1,2) = -jm(1,2) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(2,1) = -jm(2,1) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(2,2) = jm(1,1) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
   
      nxy(1,1) = (-(1.0d0 - a) * jmi(1,1) - (1.0d0 - c) * jmi(1,2))/ 4.0d0 
      nxy(1,2) = (-(1.0d0 - a) * jmi(2,1) - (1.0d0 - c) * jmi(2,2))/ 4.0d0 
      nxy(2,1) = ((1.0d0 - a) * jmi(1,1) - (1.0d0 + c) * jmi(1,2))/ 4.0d0 
      nxy(2,2) = ((1.0d0 - a) * jmi(2,1) - (1.0d0 + c) * jmi(2,2))/ 4.0d0
      nxy(3,1) = ((1.0d0 + a) * jmi(1,1) + (1.0d0 + c) * jmi(1,2))/ 4.0d0
      nxy(3,2) = ((1.0d0 + a) * jmi(2,1) + (1.0d0 + c) * jmi(2,2))/ 4.0d0
      nxy(4,1) = (-(1.0d0 + a) * jmi(1,1) + (1.0d0 - c) * jmi(1,2))/ 4.0d0
      nxy(4,2) = (-(1.0d0 + a) * jmi(2,1) + (1.0d0 - c) * jmi(2,2))/ 4.0d0
            
      bm(1,1) = nxy(1,1)
      bm(1,2) = 0.0d0
      bm(1,3) = nxy(2,1)
      bm(1,4) = 0.0d0
      bm(1,5) = nxy(3,1)
      bm(1,6) = 0.0d0
      bm(1,7) = nxy(4,1)
      bm(1,8) = 0.0d0
      bm(2,1) = 0.0d0
      bm(2,2) = nxy(1,2)
      bm(2,3) = 0.0d0
      bm(2,4) = nxy(2,2)
      bm(2,5) = 0.0d0
      bm(2,6) = nxy(3,2)
      bm(2,7) = 0.0d0
      bm(2,8) = nxy(4,2)
      bm(3,1) = nxy(1,2)
      bm(3,2) = nxy(1,1)
      bm(3,3) = nxy(2,2)
      bm(3,4) = nxy(2,1)
      bm(3,5) = nxy(3,2)
      bm(3,6) = nxy(3,1)
      bm(3,7) = nxy(4,2)
      bm(3,8) = nxy(4,1)
   end subroutine

   subroutine uthdata(b,a1,y1,y2,p1,p2,b1,alpha)
   implicit none
   real(8) b1,b,lamda1,lamda2,mu1,mu2,y1,y2,p1,p2,alpha,a1
      b1 = b * sqrt(2.0d0)
      lamda1 = y1*p1 / (1.0d0 + p1)/(1.0d0-2.0d0*p1)
      lamda2 = y2*p2 / (1.0d0 + p2)/(1.0d0-2.0d0*p2)
      mu1 = y1 / 2.0d0 /(1.0d0 + p1)
      mu2 = y2 / 2.0d0 /(1.0d0 + p2)
      alpha = ((lamda1 + mu1 + mu2) * (b1 ** 2.0d0)) / ((lamda2 + mu2) * (a1 ** 2.0d0) +&
       (lamda1 + mu1)*((b1 ** 2.0d0) - (a1 ** 2.0d0)) + mu2 * (b1 ** 2.0d0))
   end subroutine

   subroutine uthdatacheck(b,a1,y1,y2,p1,p2,b1,alpha,lamda1,mu1)
   implicit none
   real(8) b1,b,lamda1,lamda2,mu1,mu2,y1,y2,p1,p2,alpha,a1
      b1 = b * sqrt(2.0d0)
      lamda1 = y1*p1 / (1.0d0 + p1)/(1.0d0-2.0d0*p1)
      lamda2 = y2*p2 / (1.0d0 + p2)/(1.0d0-2.0d0*p2)
      mu1 = y1 / 2.0d0 /(1.0d0 + p1)
      mu2 = y2 / 2.0d0 /(1.0d0 + p2)
      alpha = ((lamda1 + mu1 + mu2) * b1 ** 2.0d0) /((lamda2 + mu2) * a1 ** 2.0d0 +&
      (lamda1 + mu1)*(b1 ** 2.0d0 - a1 ** 2.0d0) + mu2 * b1 ** 2.0d0)
   end subroutine

   subroutine uth(a1,b1,alpha,xi,yi,r,uthx,uthy)
   implicit none
   real(8) xi,yi,r,sita,uthr,uthx,uthy,alpha,a1,b1
      r = sqrt(xi ** 2.0d0 + yi ** 2.0d0)
      sita = atan2(yi, xi)
      if (r <= 3.000d0) then  !!!中心からの距離で半径方向の理論解を分けて定義
         uthr = ((1.0d0 - (b1 ** 2.0d0) / (a1 ** 2.0d0)) * alpha + (b1 ** 2.0d0) / (a1 ** 2.0d0)) * r
      else
         uthr = (r - (b1 ** 2.0d0) / r) * alpha + (b1 ** 2.0d0) / r
      end if
      uthx = uthr * cos(sita) !x方向とy方向に分解
      uthy = uthr * sin(sita)
   end subroutine

subroutine makebc(b1,n,pn,pn1,y1,y2,p1,p2,globalnum)
implicit none
integer i,j,e,ep,bc1,bc2,bc3,bc4,bc0,gr1,gr2,pn,pn1
integer,allocatable::globalnum(:,:)
real(8) alpha,lamda1,lamda2,mu1,mu2,sita,a,b,y1,p1,y2,p2,b1
real(8),allocatable::n(:,:),ux(:),uy(:),ur(:),r(:)

a = 3.00d0
b = b1 * sqrt(2.00d0)
open(32, file = "bc.dat", status = "replace")
   lamda1 = y1*p1 / (1.0d0 + p1)/(1.0d0-2.0d0*p1)
   lamda2 = y2*p2 / (1.0d0 + p2)/(1.0d0-2.0d0*p2)
   mu1 = y1 / 2.0d0 /(1.0d0 + p1)
   mu2 = y2 / 2.0d0 /(1.0d0 + p2)
   alpha = (lamda1 + mu1 + mu2) * b * b /((lamda2 + mu2) *a*a +(lamda1 + mu1)*(b*b - a*a) + mu2 *b*b)
   allocate (r(pn),ur(pn),ux(pn),uy(pn),source = 0.0d0)

   do i = 1, pn
     r(i) = sqrt(n(i,1) ** 2.0d0 + n(i,2) ** 2.0d0)
     if(r(i) <= 3.000d0) then
       ur(i) = ((1.0d0 - b * b / a / a) * alpha + b * b / a / a) * r(i)
     else
       ur(i) = (r(i) - b * b / r(i)) * alpha + b * b / r(i)
     end if
     sita = atan2(n(i,2), n(i,1))
     ux(i) = ur(i) * cos(sita)
     uy(i) = ur(i) * sin(sita)
   end do

bc1 = 0
bc2 = 0
bc3 = 0
bc4 = 0
   do i = 1,pn
      if(n(i,2) == 0.00d0 .and. r(i) < 5.999d0)then !下辺固定
         bc1 = bc1 + 1
      end if
      if(n(i,1) == 0.00d0 .and. r(i) < 5.999d0)then !左辺固定
         bc2 = bc2 + 1
      end if
      if(n(i,1) > 5.999d0 .or. n(i,2) > 5.999d0)then !上右辺引張
         bc3 = bc3 + 2
      end if
      if((i > pn1 .and. r(i) > 2.999d0) .and. (n(i,1) > 0.0001d0 .and. n(i,2) > 0.0001d0))then! 境界部固定
         bc4 = bc4 + 2
      end if
   enddo
   bc4 = bc4 + 2 !!境界上ローカル端点の半径方向ゼロ条件
   bc0 = bc1 + bc2 + bc3 + bc4
    write(32,*) bc0, 2
    !write(32,*) pn*2, 2
do i = 1, pn
   if(n(i,1) == 0.00d0 .and. r(i) < 5.999d0)then
      write(32,*) i, 1, 0.0d0
   endif
   if(n(i,2) == 0.00d0 .and. r(i) < 5.999d0 )then
      write(32,*) i, 2, 0.0d0
   endif
   if(n(i,1) > 5.999d0 .or. n(i,2) > 5.999d0) then
      write(32,*) i, 1, ux(i)
      write(32,*) i, 2, uy(i)
   endif
   if(i > pn1 .and. r(i) > 2.999d0 .and. n(i,1) > 0.001d0 .and. n(i,2) > 0.001d0)then
      write(32,*) i, 1, 0.0d0
      write(32,*) i, 2, 0.0d0
   end if
   if(i > pn1 .and. r(i) > 2.999d0 .and. n(i,1) < 0.001d0)then
      write(32,*) i, 2, 0.0d0
   end if
   if(i > pn1 .and. r(i) > 2.999d0 .and. n(i,2) < 0.001d0)then
      write(32,*) i, 1, 0.0d0
   end if
end do
close (32)
end subroutine

subroutine makeload(pn,load)
implicit none
integer i, pn, ld0
real(8),allocatable:: load(:,:)

ld0 = 0
do i = 1, pn
   if(load(i,1) /= 0.0d0)then
      ld0 = ld0 + 1
   end if
   if(load(i,2) /= 0.0d0)then
      ld0 = ld0 + 1
   end if
end do

open(32, file = "load.dat", status = "replace")
   write(32,*) ld0,2
   do i = 1, pn
      if(load(i,1) /= 0.0d0)then
         write(32,*) i, 1, load(i,1)
      end if
      if(load(i,2) /= 0.0d0)then
         write(32,*) i, 2, load(i,2)
      end if
   end do
close (32)
end subroutine

subroutine find_elem(x,y,a,b,c1,n,globalnum,cordx,cordy,en) !!x,y:任意点の座標 a,b:グローバルメッシュの長さ c1,c2:グローバルメッシュの分割数 n:グローバルメッシュの節点の座標 globalnum:要素を構成する節点番号
implicit none
integer c1, en, xx, yy
integer,allocatable::globalnum(:,:)
real(8) x,y,a,b,cordx,cordy
real(8),allocatable::n(:,:)

xx = ceiling(x / a * c1)
yy = ceiling(y / b * c1)
if(xx <= 1)then
   xx = 1
end if
if(yy <= 1)then
   yy = 1
end if
en = c1*(yy - 1) + xx !!enは任意の点を囲む要素番号
cordx = (x - n(globalnum(en,1),1)) / (a / c1) * 2.00d0 - 1.00d0 !!要素内でのx座標
cordy = (y - n(globalnum(en,1),2)) / (b / c1) * 2.00d0 - 1.00d0 !!要素内でのy座標
end subroutine

   subroutine find_enn(en,enn)
   implicit none
   integer j, enn
   integer,allocatable::en(:)
   enn = 1                                !!enn:またいでいる要素の数
      if(en(1) /= en(2))then
        enn = enn + 1
      end if
      if(en(2) /= en(3) .and. en(1) /= en(3))then
        enn = enn + 1
      end if
      if(en(1) /= en(4) .and. en(2) /= en(4) .and. en(3) /= en(4))then
        enn = enn + 1
      end if
   end subroutine

   subroutine make_surface(pn,pn1,pn2,n,surfacenum,spn,se) !!!線要素の作成
   implicit none
   integer,allocatable:: surfacenum(:,:),node2(:)
   integer pn,pn1,pn2,spn,i,j,imin,se
   real(8) d,dmin,r
   real(8),allocatable:: n(:,:),sn(:,:)

   j = 0
   do i = pn1+1, pn
      r = sqrt(n(i,1) ** 2.0d0 + n(i,2) ** 2.0d0)
      if(r > 2.9999d0)then
         j = j + 1
      end if
   end do
   spn = j
   allocate(surfacenum(spn-1,2))
   allocate(node2(spn),sn(spn,2))
   j = 1
   do i = pn1+1, pn
      r = sqrt(n(i,1) ** 2.0d0 + n(i,2) ** 2.0d0)
      if(r > 2.9999d0)then
         sn(j,1) = n(i,1)
         sn(j,2) = n(i,2)
         node2(j) = i
         j = j + 1
      end if
   end do
   do i = 1, spn
      if(sn(i,2) == 0.0d0)then
         surfacenum(1,1) = node2(i)
      end if
   end do
   se = spn - 1
   do j = 1, se
      dmin = 1.0d0
      do i = 1, spn
         d = sn(i,2) - n(surfacenum(j,1),2)
         if(d > 0 .and. d < dmin)then
            dmin = d
            imin = i
         end if
      end do
      surfacenum(j,2) = node2(imin)
      if(j < spn-1)then
         surfacenum(j+1,1) = node2(imin)
      end if
   end do

   end subroutine

subroutine localmesh(c2)
implicit none
integer c2
open(30, file = "localmesh.geo", status = "replace")
write(30,"(a)")"// Gmsh project created on Tue Jan 18 17:16:48 2022"
write(30,"(a)")'SetFactory("OpenCASCADE");'
write(30,"(a)")"//+"
write(30,"(a)")"Point(1) = {0, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(2) = {1.01, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(3) = {3, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(4) = {3/1.41421356, 3/1.41421356, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(5) = {0, 3, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(6) = {0, 1.01, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(7) = {1.01, 1.01, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(1) = {1, 2};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(2) = {2, 3};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(3) = {5, 6};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(4) = {6, 1};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(5) = {2, 7};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(6) = {7, 6};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(7) = {7, 4};"
write(30,"(a)")"//+"
write(30,"(a)")"Circle(8) = {3, 1, 4};"
write(30,"(a)")"//+"
write(30,"(a)")"Circle(9) = {4, 1, 5};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(1) = {1, 5, 6, 4};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(1) = {1};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(2) = {2, 8, -7, -5};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(2) = {2};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(3) = {7, 9, 3, -6};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(3) = {3};"
write(30,"(a)")"//+"
write(30,"(a)")'Physical Surface("fiber", 10) = {1, 2, 3};'
write(30,"(a)")" "
write(30,"(a,1x,i8,1x,a)")'Transfinite Line "*" = ',c2, 'Using Bump 1.0;'
write(30,"(a)")'Transfinite Surface "*";'
write(30,"(a)")'Recombine Surface "*";'
write(30,"(a)")'Transfinite Volume "*";//+//+'
close(30)
end subroutine

subroutine errormesh(c2)
implicit none
integer c2
open(30, file = "errormesh.geo", status = "replace")
write(30,"(a)")"// Gmsh project created on Tue Jan 18 17:16:48 2022"
write(30,"(a)")'SetFactory("OpenCASCADE");'
write(30,"(a)")"//+"
write(30,"(a)")"Point(1) = {0, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(2) = {1.01, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(3) = {3, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(4) = {3/1.41421356, 3/1.41421356, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(5) = {0, 3, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(6) = {0, 1.01, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(7) = {1.01, 1.01, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(1) = {1, 2};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(2) = {2, 3};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(3) = {5, 6};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(4) = {6, 1};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(5) = {2, 7};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(6) = {7, 6};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(7) = {7, 4};"
write(30,"(a)")"//+"
write(30,"(a)")"Circle(8) = {3, 1, 4};"
write(30,"(a)")"//+"
write(30,"(a)")"Circle(9) = {4, 1, 5};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(1) = {1, 5, 6, 4};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(1) = {1};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(2) = {2, 8, -7, -5};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(2) = {2};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(3) = {7, 9, 3, -6};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(3) = {3};"
write(30,"(a)")"//+"
write(30,"(a)")'Physical Surface("fiber", 10) = {1, 2, 3};'
write(30,"(a)")"//+"
write(30,"(a)")"Point(8) = {6, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(9) = {6, 6, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(10) = {0, 6, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(10) = {3, 8};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(11) = {8, 9};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(12) = {9, 4};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(13) = {9, 10};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(14) = {10, 5};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(4) = {10, 11, 12, -8};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(4) = {4};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(5) = {13, 14, -9, -12};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(5) = {5};"
write(30,"(a)")"//+"
write(30,"(a)")'Physical Surface("resin", 15) = {4, 5};'
write(30,"(a)")" "
write(30,"(a,1x,i8,1x,a)")'Transfinite Line "*" =', c2, "Using Bump 1.0;"
write(30,"(a)")'Transfinite Surface "*";'
write(30,"(a)")'Recombine Surface "*";'
write(30,"(a)")'Transfinite Volume "*";//+//+'
close(30)
end subroutine

subroutine errormesh2(c2)
implicit none
integer c2
open(30, file = "errormesh2.geo", status = "replace")
write(30,"(a)")"// Gmsh project created on Tue Jan 18 17:16:48 2022"
write(30,"(a)")'SetFactory("OpenCASCADE");'
write(30,"(a)")"//+"
write(30,"(a)")"Point(1) = {0, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(2) = {1.01, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(3) = {3, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(4) = {4.5, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(5) = {6, 0, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(6) = {6, 6, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(7) = {0, 6, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(8) = {0, 4.5, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(9) = {0, 3, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(10) = {0, 1.01, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(11) = {1.01, 1.01, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(12) = {3/1.41421356, 3/1.41421356, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Point(13) = {4.5/1.41421356, 4.5/1.41421356, 0, 1.0};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(1) = {1, 2};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(2) = {2, 3};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(3) = {3, 4};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(4) = {4, 5};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(5) = {5, 6};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(6) = {6, 7};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(7) = {7, 8};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(8) = {8, 9};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(9) = {9, 10};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(10) = {10, 1};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(11) = {2, 11};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(12) = {11, 10};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(13) = {11, 12};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(14) = {12, 13};"
write(30,"(a)")"//+"
write(30,"(a)")"Line(15) = {13, 6};"
write(30,"(a)")"//+"
write(30,"(a)")"Circle(16) = {3, 1, 12};"
write(30,"(a)")"//+"
write(30,"(a)")"Circle(17) = {12, 1, 9};"
write(30,"(a)")"//+"
write(30,"(a)")"Circle(18) = {4, 1, 13};"
write(30,"(a)")"//+"
write(30,"(a)")"Circle(19) = {13, 1, 8};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(1) = {1, 11, 12, 10};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(1) = {1};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(2) = {2, 16, -13, -11};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(2) = {2};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(3) = {13, 17, 9, -12};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(3) = {3};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(4) = {3, 18, -14, -16};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(4) = {4};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(5) = {14, 19, 8, -17};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(5) = {5};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(6) = {4, 5, -15, -18};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(6) = {6};"
write(30,"(a)")"//+"
write(30,"(a)")"Curve Loop(7) = {15, 6, 7, -19};"
write(30,"(a)")"//+"
write(30,"(a)")"Plane Surface(7) = {7};"
write(30,"(a)")"//+"
write(30,"(a)")"Physical Surface('fiber', 20) = {1, 2, 3};"
write(30,"(a)")"//+"
write(30,"(a)")"Physical Surface('resin', 21) = {4, 6, 7, 5};"
write(30,"(a)")" "
write(30,"(a,1x,i8,1x,a)")'Transfinite Line "*" =', c2, "Using Bump 1.0;"
write(30,"(a)")'Transfinite Surface "*";'
write(30,"(a)")'Recombine Surface "*";'
write(30,"(a)")'Transfinite Volume "*";//+//+'
close(30)
end subroutine
end module 