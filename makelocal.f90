program main
  implicit none
  character dummy
  integer i,j,e,ep,bc1,bc2,bc3,bc0,gr1,gr2
  integer,allocatable::globalnum(:,:)
  integer(8) pn
  real(8),allocatable::x(:),y(:),ux(:),uy(:),ur(:),r(:)
  
  open(12, file="localmesh.vtk", status="old")
    read (12, *)dummy
    read (12, *)dummy
    read (12, *)dummy
    read (12, *)dummy
    read (12, *)dummy, pn, dummy
    allocate (x(pn),y(pn), source = 0.0d0)
    do i = 1, pn
       read(12, *)x(i),y(i),dummy
    end do
    read (12, *)dummy, e, dummy
    ep = 4
    allocate (globalnum(e,ep), source = 0)
    do i = 1, e
      read(12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
    enddo

    read(12, *)dummy, e
    do i = 1, e
      read(12,*) dummy
    enddo

    close (12)

    open(30, file = "nodelocal.dat", status = "replace")
    write(30,*) pn
    do i = 1, pn
      write(30,*)x(i),y(i)
    end do
    close (30)

    open(31, file = "elemlocal.dat", status = "replace")
    write(31,*) e,ep
    do i = 1, e
      write(31,*)globalnum(i,1)+1,globalnum(i,2)+1,globalnum(i,3)+1,globalnum(i,4)+1
    end do
    close (31)

end program