program main
use module
use module2
use module3
implicit none
real(8) xx,yy,a,b,cordx,cordy,p1,p2,y1,y2
integer c1,c2,en,e,e1,e2,pn,pn1,pn2,ep,ep1,ep2,i,j,enn,aa,spn,se
real(8),allocatable::n(:,:),n1(:,:),n2(:,:),kgl(:,:)
integer,allocatable::globalnum(:,:),globalnum1(:,:),globalnum2(:,:),surfacenum(:,:)
integer,pointer::indexmat(:),itemmat(:)
call read_file1(y1,p1,y2,p2,a,b,c1,c2) 
call read_file2(pn1, n1)
call read_file3(e1, ep1, globalnum1) 
call read_file22(pn2, n2)
call read_file33(e2, ep2, globalnum2)
pn = pn1 + pn2
e = e1 + e2
ep = ep1
aa = 2
allocate(n(pn,2),globalnum(e,ep))
do i = 1, pn1
  do j = 1, 2
    n(i,j) = n1(i,j)
  enddo
enddo
do i = pn1+1, pn
  do j = 1,2
    n(i,j) = n2(i-pn1,j)
  enddo
enddo
do i = 1, e1
  do j = 1, ep
    globalnum(i,j) = globalnum1(i,j)
  enddo
enddo
do i = e1+1, e
  do j = 1,ep
    globalnum(i,j) = globalnum2(i-e1,j) + pn1
  enddo
enddo
call make_surface(pn,pn1,pn2,n,surfacenum,spn,se)
do i = 1, 5
write(*,*)surfacenum(i,1),surfacenum(i,2)
!call index(a,b,c1,e1,e,pn2,n,globalnum,indexmat)
!call item(e,e1,a,b,c1,n,globalnum,indexmat,itemmat)
!call find_elem(xx,yy,a,b,c1,n,globalnum,cordx,cordy,en)
enddo
end program