module module2
    use module
    implicit none

contains
subroutine KG(aa,j,globalnum, n, y,p,y1,p1,y2,p2,d,jm,jmi,bm,Ke)
   implicit none
   integer,allocatable::globalnum(:,:)
   integer j,pn,ep,i,k,l,o,aa
   real(8):: a, c, det, p, y,p1,y1,p2,y2,xx,yy,r,nn1,nn2,nn3,nn4,xxx,yyy,gzi,ita
   real(8):: bdbdetp(8,8), bdb(8,8), db(3,8), jm(2,2), bt(8,3) = 0.0d0,&
             d(3,3), jmi(2,2), nxy(4,2) = 0.0d0, bm(3,8), Ke(8,8)
   real(8), allocatable ::n(:,:)
   
   Ke = 0.0d0
   do k = 1, aa
      do i = 1, aa
         xxx = 2.0d0*real(i)/real(aa) - 1.0d0  !!各格子の右上の点の座標
         yyy = 2.0d0*real(k)/real(aa) - 1.0d0
         do ii = 1, 4
            bdbdetp = 0.0d0
            bdb = 0.0d0
            db = 0.0d0
            call gziita(ii,gzi,ita,nn1,nn2,nn3,nn4)
            
            c = (nn1*(xxx-2.0d0/real(aa)) + nn2*xxx + nn3*xxx + nn4*(xxx-2.0d0/real(aa)))/4.0d0
            a = (nn1*(yyy-2.0d0/real(aa)) + nn2*(yyy-2.0d0/real(aa)) + nn3*yyy + nn4*yyy)/4.0d0
            call jmat(jm,c,a,n,globalnum,j)
            call Bmat(bm,nxy,jmi,jm,c,a)
            
            nn1 = (1.0d0-c)*(1.0d0-a)
            nn2 = (1.0d0+c)*(1.0d0-a)
            nn3 = (1.0d0+c)*(1.0d0+a)
            nn4 = (1.0d0-c)*(1.0d0+a)
            xx = (nn1*n(globalnum(j,1),1) + nn2*n(globalnum(j,2),1) + &  !!積分点座標
            nn3*n(globalnum(j,3),1) + nn4*n(globalnum(j,4),1)) / 4.0d0
            yy = (nn1*n(globalnum(j,1),2) + nn2*n(globalnum(j,2),2) + &
            nn3*n(globalnum(j,3),2) + nn4*n(globalnum(j,4),2)) / 4.0d0
            r = sqrt(xx ** 2.0d0 + yy ** 2.0d0)
            if(r < 3.0001d0)then
               y = y1
               p = p1
            else
               y = y2
               p = p2
            end if
            call Dmat(d,y,p)

            do o = 1, 3
               do q = 1, 8
                  bt(q,o) = bm(o,q) !btを定義
               end do
            end do
         
            do o = 1, 3
               do q = 1, 8
                  do w = 1, 3
                     db(o,q) = db(o,q) + d(o,w) * bm(w,q) !dbを計算
                  end do
               end do
            end do
         
            do o = 1, 8
               do q = 1, 8
                  do w = 1, 3
                     bdb(o,q) = bdb(o,q) + bt(o,w) * db(w,q) !bdbを計算
                  end do
               end do
            end do
 
            det = ((jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)))/real(aa)/real(aa) !det計算

            if(det < 0) then
               write(*,*) "det<0"
               stop 
            end if
              
            do o = 1, 8
               do q = 1, 8
                  bdbdetp(q,o) = bdb(q,o) * det !bdbdetp計算
               end do
            end do

            do o = 1,8
               do l = 1,8
                  Ke(o,l) = Ke(o,l) + bdbdetp(o,l)
               end do
            end do
         end do
      end do
   end do
end subroutine KG

    subroutine Kesub(j,globalnum, n, y,p,y1,p1,y2,p2,d,jm,jmi,bm,Ke)
    implicit none
    integer,allocatable::globalnum(:,:)
    integer j,pn,ep,i,k,l,o,aa
    real(8):: a, c, det, p, y,p1,y1,p2,y2,xx,yy,r,nn1,nn2,nn3,nn4
    real(8):: bdbdetp(8,8), bdb(8,8), db(3,8), jm(2,2), bt(8,3) = 0.0d0,&
             d(3,3), jmi(2,2), nxy(4,2) = 0.0d0, bm(3,8), Ke(8,8)
    real(8), allocatable ::n(:,:)
   
    Ke = 0.0d0
        do ii = 1, 4
            bdbdetp = 0.0d0
            bdb = 0.0d0
            db = 0.0d0
            call gziita(ii,c,a,nn1,nn2,nn3,nn4)
            call jmat(jm,c,a,n,globalnum,j)
            call Bmat(bm,nxy,jmi,jm,c,a)
            xx = (nn1*n(globalnum(j,1),1) + nn2*n(globalnum(j,2),1) + &  !!積分点座標
            nn3*n(globalnum(j,3),1) + nn4*n(globalnum(j,4),1)) / 4.0d0
            yy = (nn1*n(globalnum(j,1),2) + nn2*n(globalnum(j,2),2) + &
            nn3*n(globalnum(j,3),2) + nn4*n(globalnum(j,4),2)) / 4.0d0
            r = sqrt(xx ** 2.0d0 + yy ** 2.0d0)
            if(r < 3.0001d0)then
               y = y1
               p = p1
            else
               y = y2
               p = p2
            end if
            call Dmat(d,y,p)
            do o = 1, 3
               do q = 1, 8
                  bt(q,o) = bm(o,q) !btを定義
               end do
            end do
         
            do o = 1, 3
               do q = 1, 8
                  do w = 1, 3
                     db(o,q) = db(o,q) + d(o,w) * bm(w,q) !dbを計算
                  end do
               end do
            end do
         
            do o = 1, 8
               do q = 1, 8
                  do w = 1, 3
                     bdb(o,q) = bdb(o,q) + bt(o,w) * db(w,q) !bdbを計算
                  end do
               end do
            end do
 
            det = (jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)) !det計算

            if(det < 0) then
               write(*,*) "det<0"
               stop 
            end if
              
            do o = 1, 8
               do q = 1, 8
                  bdbdetp(q,o) = bdb(q,o) * det !bdbdetp計算
               end do
            end do

            do o = 1,8
               do l = 1,8
                  Ke(o,l) = Ke(o,l) + bdbdetp(o,l)
               end do
            end do
        end do
    end subroutine

    subroutine KGLmat(aa,k,l,i,j,y,p,y1,p1,globalnum,n,a,b,c1,en,KGL) !!!module.f90を用いてKGLマトリクス作成
        implicit none
        integer,allocatable::globalnum(:,:)
        integer e,pn,ep,i,j,c1,o,q,w,k,l,en,aa
        real(8) ita, gzi,p,y,a,b,cordx,cordy,xx,yy,det,y1,p1,nn1,nn2,nn3,nn4,xxx,yyy,aaa,ccc
        real(8) d(3,3), bg(3,8), bl(3,8), bdb(8,8), db(3,8), jm(2,2), bgt(8,3),&
         jmi(2,2), nxy(4,2)
        real(8), allocatable ::n(:,:),KGL(:,:)
            KGL = 0.0d0
            bdb = 0.0d0
            db = 0.0d0

            y = y1
            p = p1
            call Dmat(d,y,p)

            xxx = 2.0d0*real(l)/real(aa) - 1.0d0  !!各格子の右上の点の座標
            yyy = 2.0d0*real(k)/real(aa) - 1.0d0
            !!BL
            
            call gziita(i,gzi,ita,nn1,nn2,nn3,nn4)
            ccc = (nn1*(xxx-2.0d0/real(aa)) + nn2*xxx + nn3*xxx + nn4*(xxx-2.0d0/real(aa)))/4.0d0
            aaa = (nn1*(yyy-2.0d0/real(aa)) + nn2*(yyy-2.0d0/real(aa)) + nn3*yyy + nn4*yyy)/4.0d0
            call jmat(jm,ccc,aaa,n,globalnum,j)
            call Bmat(bl,nxy,jmi,jm,gzi,ita)
            
            det = ((jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)))/real(aa)/real(aa) !det計算
            !!BG
            !!積分点ローカル座標
            nn1 = (1.0d0-ccc)*(1.0d0-aaa)
            nn2 = (1.0d0+ccc)*(1.0d0-aaa)
            nn3 = (1.0d0+ccc)*(1.0d0+aaa)
            nn4 = (1.0d0-ccc)*(1.0d0+aaa)
            xx = (nn1*n(globalnum(j,1),1) + nn2*n(globalnum(j,2),1) +&
             nn3*n(globalnum(j,3),1) + nn4*n(globalnum(j,4),1)) / 4.0d0
            yy = (nn1*n(globalnum(j,1),2) + nn2*n(globalnum(j,2),2) +&
             nn3*n(globalnum(j,3),2) + nn4*n(globalnum(j,4),2)) / 4.0d0
            call find_elem(xx,yy,a,b,c1,n,globalnum,cordx,cordy,en)
            gzi = cordx
            ita = cordy
            call jmat(jm,gzi,ita,n,globalnum,en)
            call Bmat(bg,nxy,jmi,jm,gzi,ita)
            
            do o = 1, 3
                do q = 1, 8
                    bgt(q,o) = bg(o,q) !bgtを定義
                end do
            end do

            do o = 1, 3
                do q = 1, 8
                    do w = 1, 3
                        db(o,q) = db(o,q) + d(o,w) * bl(w,q) !dbを計算
                    end do
                end do
            end do
         
            do o = 1, 8
                do q = 1, 8
                    do w = 1, 3
                        bdb(o,q) = bdb(o,q) + bgt(o,w) * db(w,q) !bdbを計算
                    end do
                end do
            end do
 
            if(det < 0) then
                write(*,*) "det<0"
                stop 
            end if
              
            do o = 1, 8
                do q = 1, 8
                    KGL(q,o) = bdb(q,o) * det !bdbdetp計算
                end do
            end do
    end subroutine KGLmat

    subroutine enmatome(j,a,b,c1,c2,n,globalnum,enen) !!積分点の位置のグローバル要素番号を要素ごとにまとめる。ここで分割数を変える必要あり。
    implicit none
    integer i,j,c1,c2,en,enen(4)
    integer,allocatable::globalnum(:,:)
    real(8) ita,gzi,a,b,cordx,cordy,xx,yy,nn1,nn2,nn3,nn4
    real(8),allocatable::n(:,:)
    
            do i = 1, 4
                call gziita(i,gzi,ita,nn1,nn2,nn3,nn4)
                xx = (nn1*n(globalnum(j,1),1) + nn2*n(globalnum(j,2),1) +&
                nn3*n(globalnum(j,3),1) + nn4*n(globalnum(j,4),1)) / 4.0d0
                yy = (nn1*n(globalnum(j,1),2) + nn2*n(globalnum(j,2),2) +&
                nn3*n(globalnum(j,3),2) + nn4*n(globalnum(j,4),2)) / 4.0d0
                call find_elem(xx,yy,a,b,c1,n,globalnum,cordx,cordy,en)
                enen(i) = en
            end do
    end subroutine

    subroutine add_u(pn,pn1,n,a,b,c1,globalnum,u)
    implicit none
    integer i,e,e1,e2,pn,pn1,pn2,en,c1
    integer,allocatable:: globalnum(:,:)
    real(8)gzi,ita,a,b,xx,yy,nn1,nn2,nn3,nn4
    real(8),allocatable:: u(:),n(:,:)

    do i = pn1+1, pn
        xx = n(i,1)
        yy = n(i,2)
        call find_elem(xx,yy,a,b,c1,n,globalnum,gzi,ita,en)
        nn1 = (1.0d0-gzi)*(1.0d0-ita)
        nn2 = (1.0d0+gzi)*(1.0d0-ita)
        nn3 = (1.0d0+gzi)*(1.0d0+ita)
        nn4 = (1.0d0-gzi)*(1.0d0+ita)
        u(i*2-1) = u(i*2-1) + (nn1*u(globalnum(en,1)*2-1) + nn2*u(globalnum(en,2)*2-1) +&
             nn3*u(globalnum(en,3)*2-1) + nn4*u(globalnum(en,4)*2-1)) / 4.0d0
        u(i*2) = u(i*2) + (nn1*u(globalnum(en,1)*2) + nn2*u(globalnum(en,2)*2) +&
             nn3*u(globalnum(en,3)*2) + nn4*u(globalnum(en,4)*2)) / 4.0d0
    end do
    end subroutine

subroutine sfemerror(aa,y1,y2,p1,p2,b,n,e3,e1,e2,globalnum,u,el2,el22,a2,n3,globalnum3,c1)
implicit none
integer i,j,e,e1,e2,e3,en,c1,k,l,aa
integer,allocatable::globalnum(:,:),globalnum3(:,:)
real(8) r,det,ec,em,eec,eem,sita,a,c,a1,b1,y1,y2,xi,yi,p1,p2,gzi,ita,a2,nn1,nn2,nn3,nn4,xxx,yyy,b,el2
real(8) jm(2,2),lamda1,lamda2,mu1,mu2,alpha,uthr,uthx,uthy,uxfem,uyfem
real(8),allocatable:: n(:,:),u(:),n3(:,:),el22(:)

em = 0.0d0
ec = 0.0d0
a1 = 3.00d0 !小円の半径
call uthdata(b,a1,y1,y2,p1,p2,b1,alpha)
allocate(el22(e3),source = 0.0d0)
do i = 1, e3  !要素ループ
    eec = 0.0d0
    eem = 0.0d0
    do k = 1, aa
        do l = 1, aa
            xxx = 2.0d0*real(l)/real(aa) - 1.0d0  !!各格子の右上の点の座標
            yyy = 2.0d0*real(k)/real(aa) - 1.0d0
            do j = 1,4  !積分点ループ
                call gziita(j,c,a,nn1,nn2,nn3,nn4)
                gzi = (nn1*(xxx-2.0d0/real(aa)) + nn2*xxx + nn3*xxx + nn4*(xxx-2.0d0/real(aa)))/4.0d0
                ita = (nn1*(yyy-2.0d0/real(aa)) + nn2*(yyy-2.0d0/real(aa)) + nn3*yyy + nn4*yyy)/4.0d0
                nn1 = (1.0d0-gzi)*(1.0d0-ita) !!格子点内部の積分点形状関数
                nn2 = (1.0d0+gzi)*(1.0d0-ita)
                nn3 = (1.0d0+gzi)*(1.0d0+ita)
                nn4 = (1.0d0-gzi)*(1.0d0+ita)

                xi = (nn1*n3(globalnum3(i,1),1) + nn2*n3(globalnum3(i,2),1) + &  !!積分点の座標
                nn3*n3(globalnum3(i,3),1) + nn4*n3(globalnum3(i,4),1)) / 4.0d0
                yi = (nn1*n3(globalnum3(i,1),2) + nn2*n3(globalnum3(i,2),2) + &
                nn3*n3(globalnum3(i,3),2) + nn4*n3(globalnum3(i,4),2)) / 4.0d0
                call uth(a1,b1,alpha,xi,yi,r,uthx,uthy) !積分点での理論解
                if(i <= e2)then !if (r <= 3.000d0) then  !!!中心から積分点までの距離で数値解の補完方法を分類
                    e = e1 + i
                    uxfem = (nn1*u(globalnum(e,1)*2-1) + nn2*u(globalnum(e,2)*2-1) + &  !!積分点での解析解を補完
                        nn3*u(globalnum(e,3)*2-1) + nn4*u(globalnum(e,4)*2-1)) / 4.0d0
                    uyfem = (nn1*u(globalnum(e,1)*2) + nn2*u(globalnum(e,2)*2) + &
                        nn3*u(globalnum(e,3)*2) + nn4*u(globalnum(e,4)*2)) / 4.0d0
                else
                    call find_elem(xi,yi,a2,b,c1,n,globalnum,gzi,ita,en)!!積分点を囲むグローバル要素
                    nn1 = (1.0d0-gzi)*(1.0d0-ita)
                    nn2 = (1.0d0+gzi)*(1.0d0-ita)
                    nn3 = (1.0d0+gzi)*(1.0d0+ita)
                    nn4 = (1.0d0-gzi)*(1.0d0+ita)
                    uxfem = (nn1*u(globalnum(en,1)*2-1) + nn2*u(globalnum(en,2)*2-1) + &  !!積分点での解析解を補完
                            nn3*u(globalnum(en,3)*2-1) + nn4*u(globalnum(en,4)*2-1)) / 4.0d0
                    uyfem = (nn1*u(globalnum(en,1)*2) + nn2*u(globalnum(en,2)*2) + &
                            nn3*u(globalnum(en,3)*2) + nn4*u(globalnum(en,4)*2)) / 4.0d0
                end if
                call jmat(jm,c,a,n3,globalnum3,i)
                det = ((jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)))/real(aa)/real(aa)
                eec = eec + ((uthx - uxfem) ** 2.0d0 + (uthy - uyfem) ** 2.0d0) * det  
                eem = eem + (uthx ** 2.0d0 + uthy ** 2.0d0) * det
                ec = ec + ((uthx - uxfem) ** 2.0d0 + (uthy - uyfem) ** 2.0d0) * det  
                em = em + (uthx ** 2.0d0 + uthy ** 2.0d0) * det
            end do
        end do
   end do
   el22(i) = sqrt(eec) / sqrt(eem)
end do
el2 = sqrt(ec) / sqrt(em)
end subroutine

subroutine additional(e,y1,y2,p1,p2,globalnum,n,u,eps,sgm)
implicit none
integer i, j, l, e
integer,allocatable::globalnum(:,:)
real(8), allocatable::u(:),n(:,:),eps(:,:),sgm(:,:)
real(8) ::bm(3,8) = 0.0d0,d(3,3),jm(2,2) = 0.0d0,jmi(2,2) = 0.0d0,nxy(4,2) = 0.0d0
real(8) y, p,a,c,nn1,nn2,nn3,nn4,y1,y2,p1,p2,xx,yy,r

      a = 0.0d0
      c = 0.0d0
      nn1 = (1.0d0-c)*(1.0d0-a)
      nn2 = (1.0d0+c)*(1.0d0-a)
      nn3 = (1.0d0+c)*(1.0d0+a)
      nn4 = (1.0d0-c)*(1.0d0+a)

      eps = 0.0d0
      sgm = 0.0d0

      do i = 1, 3
         do j = 1, e
            call jmat(jm,c,a,n,globalnum,j)
            call Bmat(bm,nxy,jmi,jm,c,a)
            do l = 1, 8
               if (mod(l,2) == 1)then
                  eps(i,j) = eps(i,j) + bm(i,l) * u(globalnum(j,(l + 1) / 2) * 2 - 1)
               else if (mod(l,2) == 0) then
                  eps(i,j) = eps(i,j) + bm(i,l) * u(globalnum(j,l / 2) * 2)
               endif
            enddo
         enddo
      enddo

      do i = 1, 3
         do j = 1, e
            xx = (nn1*n(globalnum(j,1),1) + nn2*n(globalnum(j,2),1) + &  !!積分点座標
            nn3*n(globalnum(j,3),1) + nn4*n(globalnum(j,4),1)) / 4.0d0
            yy = (nn1*n(globalnum(j,1),2) + nn2*n(globalnum(j,2),2) + &
            nn3*n(globalnum(j,3),2) + nn4*n(globalnum(j,4),2)) / 4.0d0
            r = sqrt(xx ** 2.0d0 + yy ** 2.0d0)
            if(r < 3.0001d0)then
               y = y1
               p = p1
            else
               y = y2
               p = p2
            end if
            call Dmat(d,y,p)
            do l = 1, 3
               if (mod(l,2) == 1)then
                  sgm(i,j) = sgm(i,j) + d(i,l) * eps(l,j)
               else if (mod(l,2) == 0) then
                  sgm(i,j) = sgm(i,j) + d(i,l) * eps(l,j)
               endif
            enddo
         enddo
      enddo
end subroutine

!!!法線方向を計算するサブルーチン
subroutine find_n(x1,y1,x2,y2,nvx,nvy)
implicit none
real(8) x1,y1,x2,y2,ax,ay,a,nvx,nvy 
   ax = x2 - x1
   ay = y2 - y1
   a = sqrt(ax ** 2.0d0 + ay ** 2.0d0)
   nvx = sqrt(ay ** 2.0d0 / a ** 2.0d0)  !!法線方向単位ベクトル
   nvy = sqrt(ax ** 2.0d0 / a ** 2.0d0)
   !write(*,*)nvx,nvy
end subroutine

subroutine sekibunten(e,globalnum,n,gzi,ita,xx,yy)
implicit none
integer,allocatable:: globalnum(:,:)
integer e
real(8)nn1,nn2,nn3,nn4,gzi,ita,xx,yy
real(8),allocatable:: n(:,:)
      nn1 = (1.0d0-gzi)*(1.0d0-ita)
      nn2 = (1.0d0+gzi)*(1.0d0-ita)
      nn3 = (1.0d0+gzi)*(1.0d0+ita)
      nn4 = (1.0d0-gzi)*(1.0d0+ita)
      xx = (nn1*n(globalnum(e,1),1) + nn2*n(globalnum(e,2),1) + &  !!積分点座標
      nn3*n(globalnum(e,3),1) + nn4*n(globalnum(e,4),1)) / 4.0d0
      yy = (nn1*n(globalnum(e,1),2) + nn2*n(globalnum(e,2),2) + &
      nn3*n(globalnum(e,3),2) + nn4*n(globalnum(e,4),2)) / 4.0d0
end subroutine

!要素内の任意の位置で変位からひずみと応力を計算
subroutine stress(e,c,a,y1,y2,p1,p2,globalnum,n,u,eps,sgm,e1)
implicit none
integer i, j, l, e,e1
integer,allocatable::globalnum(:,:)
real(8), allocatable::u(:),n(:,:)
real(8) ::bm(3,8) = 0.0d0,d(3,3),jm(2,2),jmi(2,2),nxy(4,2),eps(3),sgm(3)
real(8) y, p,a,c,y1,y2,p1,p2

      eps = 0.0d0
      sgm = 0.0d0
      call jmat(jm,c,a,n,globalnum,e)
      call Bmat(bm,nxy,jmi,jm,c,a)
      
      do i = 1, 3
         do l = 1, 8
            if (mod(l,2) == 1)then
               eps(i) = eps(i) + bm(i,l) * u(globalnum(e,((l + 1) / 2)) * 2 - 1)
            else if (mod(l,2) == 0) then
               eps(i) = eps(i) + bm(i,l) * u(globalnum(e,(l / 2)) * 2)
            endif
         enddo
      enddo
      !write(*,*)eps(1),eps(2),eps(3)
      if(e > e1)then
         y = y1
         p = p1
      else
         y = y2
         p = p2
      end if
      call Dmat(d,y,p)
      do i = 1, 3
         do l = 1, 3
            sgm(i) = sgm(i) + d(i,l) * eps(l)
         enddo
      enddo
end subroutine

!トラクションベクトルの計算
subroutine trac(nvx,nvy,sgm,trac1)
implicit none
real(8) nvx,nvy,sgm(3),trac1(2)
trac1(1) = sgm(1) * nvx + sgm(3) * nvy
trac1(2) = sgm(3) * nvx + sgm(2) * nvy
!write(*,*)trac1(1),trac1(2)
end subroutine

!トラクションの差を計算(積分点ごと)
subroutine diff_trac(i,trac_local,trac_global,diff)
implicit none
integer i
real(8),allocatable:: trac_local(:,:), trac_global(:,:), diff(:,:)
diff(i,1) = trac_local(i,1) + trac_global(i,1)
diff(i,2) = trac_local(i,2) + trac_global(i,2)
!write(*,*)diff(i,1),diff(i,2),i
end subroutine

!積分点ごとにトラクションの差を要素の節点に配分（積分）（線要素を四辺形要素として扱う）
subroutine sekibun(e,gp,gzi,ita,diff,load,globalnum,n)
implicit none
integer i,e,gp
integer,allocatable::globalnum(:,:)
real(8) gzi,ita,nn1,nn2,nn3,nn4,det,jm(2,2)
real(8),allocatable:: diff(:,:),load(:,:),n(:,:)

nn1 = (1.0d0-gzi)*(1.0d0-ita) / 4.0d0
nn2 = (1.0d0+gzi)*(1.0d0-ita) / 4.0d0
nn3 = (1.0d0+gzi)*(1.0d0+ita) / 4.0d0
nn4 = (1.0d0-gzi)*(1.0d0+ita) / 4.0d0
call jmat(jm,gzi,ita,n,globalnum,e)
det = ((jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)))
do i = 1, 2
   load(globalnum(e,1),i) = load(globalnum(e,1),i) + nn1 * diff(gp,i) * (-1.00d0) * det
   load(globalnum(e,2),i) = load(globalnum(e,2),i) + nn2 * diff(gp,i) * (-1.00d0) * det
   load(globalnum(e,3),i) = load(globalnum(e,3),i) + nn3 * diff(gp,i) * (-1.00d0) * det
   load(globalnum(e,4),i) = load(globalnum(e,4),i) + nn4 * diff(gp,i) * (-1.00d0) * det
end do
end subroutine

!節点調整力のグローバル要素への足し込み
subroutine tasikomi(j,en,gzi,ita,globalnum,load)
implicit none
integer en,i,j
integer,allocatable::globalnum(:,:)
real(8) gzi,ita,nn1,nn2,nn3,nn4
real(8),allocatable::load(:,:)
nn1 = (1.0d0-gzi)*(1.0d0-ita) / 4.0d0
nn2 = (1.0d0+gzi)*(1.0d0-ita) / 4.0d0
nn3 = (1.0d0+gzi)*(1.0d0+ita) / 4.0d0
nn4 = (1.0d0-gzi)*(1.0d0+ita) / 4.0d0
if(load(j,1) /= 0.0d0 .or. load(j,2) /= 0.0d0)then
   do i = 1, 2
      load(globalnum(en,1),i) = load(globalnum(en,1),i) + nn1 * load(j,i)
      load(globalnum(en,2),i) = load(globalnum(en,2),i) + nn2 * load(j,i)
      load(globalnum(en,3),i) = load(globalnum(en,3),i) + nn3 * load(j,i)
      load(globalnum(en,4),i) = load(globalnum(en,4),i) + nn4 * load(j,i)
      load(j,i) = 0.0d0
   end do
end if
end subroutine

subroutine stresscheck(b,xx,yy,y1,y2,p1,p2,epsx,epsy,sgmx,sgmy)
implicit none
real(8) xx, yy, r,a,b1,b,y1,y2,p1,p2,alpha,epsr,epssita,sgmr,sgmsita,mu,lamda,&
            sgmx,sgmy,sita,epsx,epsy

r = sqrt(xx ** 2.0d0 + yy ** 2.0d0)
a = 3.0d0
call uthdatacheck(b,a,y1,y2,p1,p2,b1,alpha,lamda,mu)
epsr =    ((1.0d0 - b1 * b1 / a / a) * alpha + b1 * b1 / a / a)
epssita = ((1.0d0 - b1 * b1 / a / a) * alpha + b1 * b1 / a / a)

sgmr = 2.0d0 * mu * epsr + lamda * (epsr + epssita)
sgmsita = 2.0d0 * mu * epssita + lamda * (epsr + epssita)
sita = atan2(yy, xx)
epsx = epsr * cos(sita) - epssita * sin(sita)
epsy = epsr * sin(sita) + epssita * cos(sita)
sgmx = sgmr * cos(sita) - sgmsita * sin(sita)
sgmy = sgmr * sin(sita) + sgmsita * cos(sita)
end subroutine

end module